create database epanalhpsh;
use epanalhpsh;

create table professor (
ID integer,
fname varchar(10),
lname varchar(20),
salary float,
position varchar(20),
dno integer,
primary key(ID));


create table department(
Dnumber integer,
dname varchar(15),
address varchar(30),
M_ID integer,
primary key(Dnumber),
constraint foreign key(M_ID) references professor(ID));

alter table professor add constraint foreign key(dno) references department(dnumber);

insert into professor values(3, 'Nikos','Vaios',1220.87,'Assistant Professor',NULL);
insert into professor values(4, 'kostas','Papas',1000.98,'Lecture',NULL);
insert into professor values(7, 'Manolis','Veras',1500.23,'Associate Professor',NULL);
insert into professor values(8, 'Maria','Lamprou',1678.23,'Professor',NULL);
insert into professor values(9, 'Stella','Petraki',1102.34,'Lecture',NULL);
insert into professor values(10, 'Manolis','Pateros',1200.33,'Assistant Professor',NULL);
insert into professor values(11, 'Eirini','Kafatou',1498.34,'Associate Professor',NULL);

insert into Department values(1,'Chemistry','Khpoupoli 3',8);
insert into Department values(2,'Computer Science','Ikarou 23',7);

update professor
set dno=1
where ID=3 OR id=8 OR ID=9 OR ID=10;

update professor
set dno=2
where ID=4 OR id=7 OR ID=11;






select fname,lname
from professor
where salary in (select max(salary)
	         from professor);

select fname,lname
from professor
where salary>=all(select salary
	         from professor);

select position, count(*),avg(salary)
from professor
group by position;

select fname,lname
from professor,department
where dname='Chemistry' and M_ID=ID;